import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by MaksSklyarov on 06.04.17.
 */
public class ClientThread extends Thread {
    private String addres;
    private int port;
    private  Socket socket;
    private LogWriter log;

    private Scanner keyboard = new Scanner(System.in);
    MainWindow mw;



    public ClientThread(String addres, int port,MainWindow mw) throws IOException {
        this.addres = addres;
        this.port = port;
        this.mw = mw;
        this.log = new LogWriter("log.txt");


    }
    public Socket getSocket(){
        return socket;
    }


    public void run() {


            try {

//                socket.getOutputStream().flush();
                InetAddress inetAddress = InetAddress.getByName(addres);
                socket = new Socket(inetAddress, port);
               String name = JOptionPane.showInputDialog(null,"Input username");
                //Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
                //BufferedImage capture = new Robot().createScreenCapture(screenRect);
                //socket.getOutputStream().flush();
                //ImageIO.write(capture, "png", socket.getOutputStream());
               // socket.getOutputStream().flush();
                OutputStreamWriter outputStream = new OutputStreamWriter(socket.getOutputStream());
                InputStreamReader inputStream = new InputStreamReader(socket.getInputStream());
                Scanner in = new Scanner(inputStream);
                final PrintWriter out = new PrintWriter(outputStream, true);
                out.println("/register "+name);

                System.out.println("Connected");
                log.Log("Connected to Server");





                String line = null;
                System.out.println("Type in something and press enter. Will send it to the server and tell ya what it thinks.");
                mw.button1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        out.println(mw.textField1.getText());
                        log.Log("[" + new java.util.Date().toString()+"]" + "Отправил сообщение на сервер: " + mw.textField1.getText());
                        mw.textField1.setText("");
                    }
                });
                mw.button1.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        int key = e.getKeyCode();
                        if(key == KeyEvent.VK_ENTER){
                            out.println(mw.textField1.getText());
                            log.Log("[" + new java.util.Date().toString()+"]" + "Отправил сообщение на сервер: " + mw.textField1.getText());
                            mw.textField1.setText("");
                        }
                    }
                });
                mw.textField1.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        int key = e.getKeyCode();
                        if(key == KeyEvent.VK_ENTER){
                            out.println(mw.textField1.getText());
                            log.Log("[" + new java.util.Date().toString()+"]" + "Отправил сообщение на сервер: " + mw.textField1.getText());
                            mw.textField1.setText("");
                        }
                    }
                });
                while (true) {

                    if (in.hasNextLine()) {




                        mw.setTextArea(mw.getTextArea() + "\n" + in.nextLine(), 2);


                    }
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

    }
        }


