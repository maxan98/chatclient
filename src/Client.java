import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by MaksSklyarov on 04.04.17.
 */
public class Client {
    public static void main(String[] args) {
        String addres = "127.0.0.1";
        int port = 7777;
        MainWindow mw = new MainWindow();
        mw.setLocationRelativeTo(null);

        mw.pack();
        mw.setVisible(true);

        ClientThread listener = null;
        try {
            listener = new ClientThread(addres,port,mw);
        } catch (IOException e) {
            e.printStackTrace();
        }
        listener.start();






    }
}
